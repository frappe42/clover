#!/bin/bash
# This script installs the base arch system.

# Check if system is booted in UEFI mode.
if [ ! -d "/sys/firmware/efi/efivars" ]
then
    # If not, then exit installer.
    echo "[Error!] System is not booted in UEFI mode. Please boot in UEFI mode & try again."
    exit 9999
fi

# Delete partition table.
wipefs -a -f /dev/nvme0n1

# Partition disks :
# 1. /dev/nvme0n1p1 for EFI  partition taking +512M.
# 2. /dev/nvme0n1p2 for swap partition taking +6GB.
# 3. /dev/nvme0n1p3 for /    partition taking rest of the disk.
( 
echo n       # Create new partition (for EFI).
echo p       # Set partition type to primary.
echo         # Set default partition number.
echo         # Set default first sector.
echo +512M   # Set +512M as last sector.
echo n       # Create new partition (for swap).
echo p       # Set partition type to primary.
echo         # Set default partition number.
echo         # Set default first sector.
echo +5722M  # Set +6GB as last sector.
echo n       # Create new partition (for root).
echo p       # Set partition type to primary.
echo         # Set default partition number.
echo         # Set default first sector.
echo         # Set default last sector (rest of the disk).
echo w       # write changes.
) | fdisk /dev/nvme0n1 -w always -W always

# Format the created partitions :

mkfs.fat -F32 /dev/nvme0n1p1 # EFI  partition
mkswap        /dev/nvme0n1p2 # Swap partition 
mkfs.ext4     /dev/nvme0n1p3 # Root partition

# Mount the filesystem.
mount /dev/nvme0n1p3 /mnt

# Update mirrorlist
# reflector --latest 20 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

# Install essential packages.
pacstrap /mnt base base-devel linux linux-firmware nano

# Generate fstab file.
genfstab -U /mnt >> /mnt/etc/fstab

# Download setup.sh from repo into /mnt.
curl https://gitlab.com/frappe42/clover/-/raw/main/setup.sh -o /mnt/setup.sh

# Execute setup.sh from /mnt with arch-chroot.
arch-chroot /mnt sh setup.sh

# Remove downloaded script
rm install.sh

# Unmount partition & reboot.
umount -R /mnt
reboot
