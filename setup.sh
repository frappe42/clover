#!/bin/bash
# This script sets up arch for everyday use.

# Sync database.
pacman -Sy

setTime () {
    # Set timezone
    ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

    # Enable network time sync
    timedatectl set-ntp true

    # Set the Hardware Clock from the System Clock
    hwclock --systohc

    # Show time status
    timedatectl status
}

setLocale () {
    # Install fonts
    pacman -S noto-fonts noto-fonts-extra awesome-terminal-fonts ttf-roboto-mono --noconfirm

    # Uncomment required locales from '/etc/locale.gen'.
    sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen

    # Generate locale.
    locale-gen

    # Set system locale. (creates locale.conf)
    localectl set-locale LANG=en_US.UTF-8

    # Show locale status
    localectl status
}

setHosts () {
    # Create the hostname file.
    echo "Specify hostname. This will be used to identify your machine on a network."
    read hostName; echo $hostName > /etc/hostname

    # Add matching entries to '/etc/hosts'.
    # ( If the system has a permanent IP address, it should be used instead of 127.0.1.1 )
    echo -e 127.0.0.1'\t'localhost'\n'::1'\t\t'localhost'\n'127.0.1.1'\t'$hostName >> /etc/hosts
}

setUser () {
    # Add regular user.
    echo "Specify username. This will be used to identify your account on this machine."
    read userName;
    useradd -m -G wheel -s /bin/bash $userName

    # Set password for new user.
    echo "Specify password for regular user : $userName."
    passwd $userName

    # Enable sudo for wheel group.
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

    # Create directories for user.
    pacman -S xdg-user-dirs --noconfirm; xdg-user-dirs-update

    # Set the root password.
    echo "Specify root password. This will be used to authorize root commands."
    passwd
}

setGrub () {
    # Install required packages.
    pacman -S grub efibootmgr --noconfirm

    # Create directory to mount EFI partition.
    mkdir /boot/efi

    # Mount the EFI partition.
    mount /dev/nvme0n1p1 /boot/efi

    # Install grub.
    grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi

    # Generate grub config.
    grub-mkconfig -o /boot/grub/grub.cfg
}

setPackages () {
    # file manager
    pacman -S pcmanfm-gtk3 xarchiver unzip mtpfs libmtp gvfs gvfs-mtp android-tools android-udev --noconfirm

    apps=(

    # essentials
    'gedit'                             # text editor
    'evince'                            # doc viewer
    'ristretto'                         # image viewer
    'firefox'                           # primary browser
    'firefox-ublock-origin'             # ad blocker
    'firefox-decentraleyes'             # local cdn emulation
    'firefox-extension-privacybadger'   # tracking blocker
    'chromium'                          # secondary browser
    'torbrowser-launcher'               # tor launcher
    'vlc'                               # media player
    'foliate'                           # ebook reader
    'fragments'                         # torrent client
    'gnome-screenshot'                  # screenshot tool
    'gnome-calculator'                  # calculator
    'gnome-disk-utility'                # disks app
    'gnome-multi-writer'                # iso file writer
    'gnome-sound-recorder'              # sound recorder
    'gnome-podcasts'                    # podcasts app
    'curtail'                           # image compressor
    'gcolor3'                           # color picker
    'kolourpaint'                       # image editor
    'mypaint'                           # raster painting
    'gimp'                              # image manipulation
    'peek'                              # GIF recorder
    'obs-studio' 		                # studio app
    'pitivi' 		                    # video editor
    'seahorse'                          # encryption keys
    'pambase'                           # PAM services
    'flatpak'                           # app sandboxing
    
    # cli
    'exa'                               # ls  alternative
    'bat'                               # cat alternative
    'git'                               # git version control
    'jq'                                # json processor
    'acpi'                              # battery client
    'brightnessctl'                     # brightness control
    'pandoc' 		                    # markup converter
    'htop'                              # process viewer
    'dust'                              # disk usage
    'fd'                                # find alternative
    'rdiff-backup'                      # backup tool
    )

    for app in "${apps[@]}"; do
        pacman -S "$app" --noconfirm
    done

    apps=(
        'com.github.geigi.cozy'         # audio book player
        'org.onlyoffice.desktopeditors' # only office suite
        'com.belmoussaoui.Decoder'      # QR decoder
    )

    for app in "${apps[@]}"; do
        flatpak install -y --noninteractive flathub "$app"
    done

}

setEditor () {
    # Install packages
    pacman -S neovim nodejs npm --noconfirm

    # Set neovim as default editor
    echo 'export VISUAL=nvim' | tee -a /etc/profile
    echo 'export EDITOR=$VISUAL' | tee -a /etc/profile
}

setShell () {
    # Install packages
    pacman -S xclip terminator fish fisher tldr --noconfirm

    # Fix hostname
    sed -i 's/hostname/uname -n/' /home/$userName/.config/fish/functions/fish_prompt.fish

    # Set fish as default shell
    chsh --shell /bin/fish $userName
}

setGUI () {
    # Install display server
    pacman -S xorg --noconfirm

    # display manager
    pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm
    systemctl enable lightdm

    # desktop environment
    pacman -S i3-gaps i3blocks lxappearance dunst rofi feh gnome-themes-extra papirus-icon-theme --noconfirm
}

setConfig () {
    # Download dot files into their desired paths.
    repo="https://gitlab.com/frappe42/clover/-/raw/main"

    # lightdm
    mkdir -p /etc/lightdm; curl "$repo"/.config/lightdm/lightdm-gtk-greeter.conf -o /etc/lightdm/lightdm-gtk-greeter.conf

    # i3
    mkdir -p /home/$userName/.config/i3; curl "$repo"/.config/i3/config -o /home/$userName/.config/i3/config
    mkdir -p /home/$userName/Pictures; curl "$repo"/assets/wallpaper -o /home/$userName/Pictures/wallpaper

    # i3blocks
    mkdir -p /home/$userName/.config/i3; curl "$repo"/.config/i3/i3blocks.conf -o /home/$userName/.config/i3/i3blocks.conf

    # rofi
    mkdir -p /home/$userName/.config/rofi; curl "$repo"/.config/rofi/config.rasi -o /home/$userName/.config/rofi/config.rasi

    # terminator
    mkdir -p /home/$userName/.config/terminator; curl "$repo"/.config/terminator/config -o /home/$userName/.config/terminator/config

    # fish
    mkdir -p /home/$userName/.config/fish/functions; curl "$repo"/.config/fish/config.fish -o /home/$userName/.config/fish/config.fish
    curl "$repo"/.config/fish/functions/fish_greeting.fish -o /home/$userName/.config/fish/functions/fish_greeting.fish

    # Reset permissions
    chown -R $userName /home/$userName/.config
    chown -R :$userName /home/$userName/.config
    chown -R $userName /home/$userName/Pictures
    chown -R :$userName /home/$userName/Pictures
}

setAudio () {
    # Install audio.
    pacman -S pulseaudio pulseaudio-alsa pulseaudio-bluetooth pavucontrol --noconfirm
}

setNetwork () {
    # Install network.
    pacman -S networkmanager --noconfirm
    systemctl enable NetworkManager
}

setPower () {
    pacman -S tlp --noconfirm
    systemctl enable tlp.service
    systemctl mask systemd-rfkill.service
    systemctl mask systemd-rfkill.socket
}

setBluetooth () {
    # Install bluetooth.
    pacman -S blueman bluez bluez-utils --noconfirm
    lsmod | grep btusb
    rfkill unblock bluetooth
    systemctl enable bluetooth.service
}

setHealth () {
    # Install firewall.
    pacman -S gufw --noconfirm

    # Enable TRIM for SSDs
    systemctl enable fstrim.timer
}

# Install & configure.
setTime
setLocale
setHosts
setUser
setGrub
setPackages
setEditor
setShell
setGUI
setConfig
setAudio
setNetwork
setPower
setBluetooth
setHealth


# Remove downloaded script & exit.
rm setup.sh
exit
